/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.udea.servlet;

import com.udea.ejb.StudentFacadeLocal;
import com.udea.entity.Student;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mejia
 */
public class StudentServlet extends HttpServlet {

    @EJB
    private StudentFacadeLocal studentFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String action =request.getParameter("action");
            String url="index.jsp";
            if("list".equals(action)){
                List<Student> findAll = studentFacade.findAll();
                request.getSession().setAttribute("student",findAll);
                url="listStudents.jsp";
            } else if ("login".equals(action)){
                String u=request.getParameter("username");
                String p=request.getParameter("password");
                boolean checkLogin=studentFacade.checkLogin(u, p);
                if(checkLogin){
                    request.getSession().setAttribute("login",u );
                    url="manager.jsp";
                }else{
                    url="login.jsp?error=1";
                }
            } else if ("insert".equals(action)){
                Student a= new Student();
                a.setUsername(request.getParameter("username"));
                a.setPassword(request.getParameter("password"));
                a.setEmail(request.getParameter("email"));
                a.setId(Integer.parseInt(request.getParameter("id")));
                a.setName(request.getParameter("name"));
                a.setLastname(request.getParameter("lastname"));
                a.setAcademicProgram(request.getParameter("academic_program"));
                if(request.getParameter("image").equals("")){
                    a.setImage("https://i.pinimg.com/originals/9f/81/2d/9f812d4cf313e887ef99d8722229eee1.jpg");
                } else {
                    a.setImage(request.getParameter(null));
                }
                studentFacade.create(a);
                url="login.jsp";
            } else if ("search".equals(action)){
                int id = Integer.parseInt(request.getParameter("id"));
                Student a = studentFacade.find(Integer.valueOf(id));
                request.getSession().setAttribute("student",a);
                url="studentData.jsp";
            } else if ("update".equals(action)) {
                String id = request.getParameter("id");
                Student a = studentFacade.find(Integer.valueOf(id));
                a.setUsername(request.getParameter("username"));
                a.setPassword(request.getParameter("password"));
                a.setEmail(request.getParameter("email"));
                a.setName(request.getParameter("name"));
                a.setLastname(request.getParameter("lastname"));
                a.setAcademicProgram(request.getParameter("academic_program"));
                if(request.getParameter("image").equals("")){
                    a.setImage("https://i.pinimg.com/originals/9f/81/2d/9f812d4cf313e887ef99d8722229eee1.jpg");
                } else {
                    a.setImage(request.getParameter("image"));
                }
                studentFacade.edit(a);
                url="StudentServlet?action=search&id="+a.getId().toString();
            } else if ("delete".equals(action)){
                String id=request.getParameter("id");
                Student a=studentFacade.find(Integer.valueOf(id));
                studentFacade.remove(a);
                url="StudentServlet?action=list";
            } else if ("logout".equals(action)) {
                request.getSession().removeAttribute("login");
                url="login.jsp";
            }
            response.sendRedirect(url);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
