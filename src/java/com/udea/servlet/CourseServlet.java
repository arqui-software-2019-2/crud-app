/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.udea.servlet;

import com.udea.ejb.CourseFacadeLocal;
import com.udea.entity.Course;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author mejia
 */
public class CourseServlet extends HttpServlet {

    @EJB
    private CourseFacadeLocal courseFacade;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        try {
            String action =request.getParameter("action");
            String url="index.jsp";
            if("list".equals(action)){
                List<Course> findAll = courseFacade.findAll();
                request.getSession().setAttribute("course",findAll);
                url="listCourses.jsp";
            } else if ("insert".equals(action)){
                Course a= new Course();
                a.setCode(Integer.parseInt(request.getParameter("code")));
                a.setName(request.getParameter("name"));
                a.setCredits(Integer.parseInt(request.getParameter("credits")));
                courseFacade.create(a);
                url="CourseServlet?action=list";
            } else if ("update".equals(action)) {
                String code=request.getParameter("code");
                Course a=courseFacade.find(Integer.valueOf(code));
                a.setName(request.getParameter("name"));
                a.setCredits(Integer.parseInt(request.getParameter("credits")));
                courseFacade.edit(a);
                url="CourseServlet?action=list";
            } else if ("search".equals(action)){
                System.out.println("Holi");
                int code = Integer.parseInt(request.getParameter("code"));
                Course a = courseFacade.find(Integer.valueOf(code));
                request.getSession().setAttribute("course",a);
                url="courseData.jsp";
            } else if ("delete".equals(action)){
                String code=request.getParameter("code");
                Course a=courseFacade.find(Integer.valueOf(code));
                courseFacade.remove(a);
                url="CourseServlet?action=list";
            }
            response.sendRedirect(url);
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
