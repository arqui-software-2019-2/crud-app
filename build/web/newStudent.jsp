<%-- 
    Document   : newAccount
    Created on : 3/03/2020, 02:15:05 PM
    Author     : mejia
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="style.css">
        <!-- Optional theme-->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">    
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <jsp:include page="menu.jsp"></jsp:include>
        <h1>Hello World!</h1>
        <div class="container well">
            <div align="center">
            <form action="StudentServlet?action=insert" method="post">
                <table>
                    <tr>
                        <th><label><b>Username:</b></label></th>
                        <th>
                            <input type="text" placeholder="Enter Username" class="form-control" name="username" required=""/>
                        </th>
                    </tr>
                    <tr>
                        <th><label><b>Password:</b></label></th>
                        <th>
                            <input type="password" placeholder="Enter Password" class="form-control" name="password" required=""/>
                        </th>
                    </tr>
                    <tr>
                        <th><label><b>Email:</b></label></th>
                        <th>
                            <input type="email" placeholder="Enter Email" class="form-control" name="email" required=""/>
                        </th>
                    </tr>
                    <tr>
                        <th><label><b>ID:</b></label></th>
                        <th>
                            <input type="number" placeholder="Enter ID" class="form-control" name="id" required=""/>
                        </th>
                    </tr>
                    <tr>
                        <th><label><b>Name:</b></label></th>
                        <th>
                            <input type="text" placeholder="Enter Name" class="form-control" name="name" required=""/>
                        </th>
                    </tr>
                    <tr>
                        <th><label><b>Last name</b></label></th>
                        <th>
                            <input type="text" placeholder="Enter Last name" class="form-control" name="lastname" required=""/>
                        </th>
                    </tr>
                    <tr>
                        <th><label><b>Academic program:</b></label></th>
                        <th>
                            <input type="text" placeholder="Enter academic program" class="form-control" name="academic_program" required=""/>
                        </th>
                    </tr>
                    <tr>
                        <th><label><b>Imagen</b></label></th>
                        <th>
                            <input type="text" placeholder="Enter user avatar" class="form-control" name="image" />
                        </th>
                    </tr>
                    <div class="break"></div>
                    </div>
                    <tr>
                        <td colspan="2">
                            <input class="btn icon-btn btn-success" type="submit" name="action" value="Insert">
                            <span class="glyphicon glyphicon-ok-sign"></span>
                            <input class="btn icon-btn btn-lg" type="reset" name="action" value="Reset">
                            <span class="glyphicon glyphicon-remove"></span>
                        </td>
                    </tr>
                </table>    
        </form> 
            </div>
            <br>
        </div>
    </body>
</html>
