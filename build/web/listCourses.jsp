<%-- 
    Document   : listCourses
    Created on : 4/03/2020, 02:29:56 PM
    Author     : mejia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="menu.jsp"></jsp:include>
        <h1>Hello, These Are Your Courses!</h1>
        <c:forEach var="a" items="${course}">
           |<a href="CourseServlet?action=search&code=${a.code}">${a.code}</a>| |${a.name}| |${a.credits}| 
           <a onclick="return confirm('Are you sure?')" href="CourseServlet?action=delete&code=${a.code}">Delete</a>        
           <hr/>
        </c:forEach>
        
    </body>
</html>

