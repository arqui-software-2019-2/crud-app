<%-- 
    Document   : courseData
    Created on : 5/03/2020, 07:01:53 PM
    Author     : mejia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="menu.jsp"></jsp:include>
        <form action="CourseServlet?action=update&code=${course.code}" method="post">
            <h1>Información del estudiante</h1>
            <table class="tg" align="center">
                <tr>
                    <td class="tg-baqh">Code</td>
                    <td class="tg-baqh" colspan="2"><input type="text" id="change1" value=" ${course.code}"class="form-control" name="code" required="" disabled/></td>
                </tr>
                <tr>
                    <td class="tg-baqh">Name</td>
                    <td class="tg-baqh" colspan="2"><input type="text" id="change2" value=" ${course.name}"class="form-control" name="name" required=""/></td>
                </tr>
                <tr>
                    <td class="tg-baqh">Credits</td>
                    <td class="tg-baqh" colspan="2"><input type="text" id="change3" value="${course.credits}" class="form-control" name="credits" required=""/> </td>
                </tr>
            </table>
            <button type="submit" id="changeS">save</button>
        </form>
    </body>
</html>
