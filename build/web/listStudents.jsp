<%-- 
    Document   : listAccounts
    Created on : 3/03/2020, 02:13:43 PM
    Author     : mejia
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="menu.jsp"></jsp:include>
        <h1>Hello World!</h1>
        <c:forEach var="a" items="${student}">
           |<a href="StudentServlet?action=search&id=${a.id}">${a.id}</a>| |${a.username}| |${a.email}| 
           <a onclick="return confirm('Esta seguro?')" href="StudentServlet?action=delete&id=${a.id}">Delete</a>        
           <hr/>
        </c:forEach>
        
    </body>
</html>
