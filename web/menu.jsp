<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:if test="${empty login}">
|<a href="login.jsp">Login</a>|
|<a href="newStudent.jsp">Register</a>|

</c:if>

<c:if test="${not empty login}">
|<a href="StudentServlet?action=logout">Logout</a>|
|<a href="StudentServlet?action=list">List Students</a>|
|<a href="CourseServlet?action=list">List Courses</a>|
|<a href="newCourse.jsp">Create Course</a>|

</c:if>

|<a href="StudentServlet?action=about">About</a>|
<hr/>