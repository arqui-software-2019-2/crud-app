<%-- 
    Document   : studentData
    Created on : 5/03/2020, 03:38:32 PM
    Author     : mejia
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <style type="text/css">
        .tg  {border-collapse:collapse;border-spacing:0;}
        .tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
        .tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:black;}
        .tg .tg-baqh{text-align:center;vertical-align:top}
        .tg .tg-c3ow{border-color:inherit;text-align:center;vertical-align:top}
    </style>

    <script>
        function myFunction() {
            document.getElementById("change1").disabled = false;
            document.getElementById("change2").disabled = false;
            document.getElementById("change3").disabled = false;
            document.getElementById("change4").disabled = false;
            document.getElementById("change5").disabled = false;
            document.getElementById("change6").disabled = false;
            document.getElementById("change6").style.display = "";
            document.getElementById("changeS").style.display = "";
            document.getElementById("changeM").style.display = "none";
        }
        
        function myFunction2() {
            document.getElementById("change1").disabled = true;
            document.getElementById("change2").disabled = true;
            document.getElementById("change3").disabled = true;
            document.getElementById("change4").disabled = true;
            document.getElementById("change5").disabled = true;
            document.getElementById("change6").disabled = true;
            document.getElementById("change6").style.display = "none";
            document.getElementById("changeS").style.display = "none";
            document.getElementById("changeM").style.display = "";
        }
    </script>
    <body>
        <jsp:include page="menu.jsp"></jsp:include>
        <form action="StudentServlet?action=update&id=${student.id}" method="post">
            <h1>Información del estudiante</h1>
             
            <table class="tg" align="center">
                <tr>
                    <th class="tg-c3ow" rowspan="5">
                        <img src="${student.image}" alt="foto de ${student.name}" width="50%"/>
                    <input type="text" value="${student.image}" id="change6" class="form-control" name="image" disabled="" style="display:none;"/>
                </th>
                <th class="tg-c3ow"colspan="3">Nombre: 
                    <input type="text"  id="change1" value="${student.name}" class="form-control" name="name" required="" disabled/>  
                    <input type="text" value="${student.lastname}" class="form-control" id="change5" name="lastname" required="" disabled=""/> </th>
                </tr>
                <tr>
                    <td class="tg-baqh">usuario</td>
                    <td class="tg-baqh" colspan="2"> <input type="text" id="change2" value=" ${student.username}"class="form-control" name="username" required="" disabled/></td>
                </tr>
                <tr>
                    <td class="tg-baqh">id</td>
                    <td class="tg-baqh" colspan="2"> ${student.id}</td>
                </tr>
                <tr>
                    <td class="tg-baqh">programa</td>
                    <td class="tg-baqh" colspan="2">  <input type="text" id="change3" value="${student.academicProgram}" class="form-control" name="academic_program" required="" disabled/> </td>
                </tr>
                <tr>
                    <td class="tg-baqh">email</td>
                    <td class="tg-baqh" colspan="2"> <input type="text" id="change4" value=" ${student.email}" class="form-control" name="email" required="" disabled/> </td>
                </tr>
            </table>
            <input type="button" id="changeM" onclick="myFunction()" value="modificar"></input>
            <button type="submit" id="changeS" style="display:none;" >guardar</button>
        </form>
    </body>
</html>


