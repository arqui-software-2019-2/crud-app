CREATE TABLE course
(
	code INT not null primary key,
	name VARCHAR(10) not null,
	credits INT not null
);
CREATE TABLE student
(
	id INT not null primary key,
	name VARCHAR(20),
	lastname VARCHAR(20),
	username VARCHAR(20),
	password VARCHAR(16),
	academic_program VARCHAR(25),
	email VARCHAR(25) not null,
	image VARCHAR(200)
);
CREATE TABLE studentxcourse(
    studentKey INT,
    courseKey INT,
    CONSTRAINT PK_sxc PRIMARY KEY(studentKey, courseKey),
    CONSTRAINT fk_studentKey
    FOREIGN KEY (studentKey)
        REFERENCES student(id)
        ON DELETE CASCADE
        ON UPDATE CASCADE,
    CONSTRAINT fk_courseKey
    FOREIGN KEY (courseKey)
        REFERENCES course(code)
        ON DELETE CASCADE
        ON UPDATE CASCADE
);